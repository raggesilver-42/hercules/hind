/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   app.js                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/15 23:10:27 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/03/15 23:28:18 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

const axios = require('axios');
const readline = require('readline');
const fs = require('fs');
const { once } = require('events');

let client_id = process.env.CLIENT_ID;
let client_secret = process.env.CLIENT_SECRET;
let token = null;

let map = {};
let on_going = 0;

if (!client_id || !client_secret) {
	console.error('Provide CLIENT_ID and CLIENT_SECRET.');
	process.exit(1);
}

const sleep = m => new Promise(r => setTimeout(r, m));

let get_result = async function(target) {
	console.log(`Getting user ${target}`);
	try {
		let res = await axios.get(`https://api.intra.42.fr/v2/users/${target}/locations`,
			{headers: {'Authorization': `Bearer ${token}`}});

		if (res.data[0].end_at) {
			console.log(`User ${target} not logged in.`);
			on_going--;
			return ;
		}
		const host = res.data[0].host;
		let e = host.match(/e[0-9]+/)[0];
		let z = host.match(/z[0-9]+/)[0];
		let r = host.match(/r[0-9]+/)[0];
		let p = host.match(/p[0-9]+/)[0];

		if (!(e in map))
			map[e] = {};
		if (!(z in map[e]))
			map[e][z] = {};
		if (!(r in map[e][z]))
			map[e][z][r] = {};
		
		map[e][z][r][p] = target;
		on_going--;
	} catch (e) {
		on_going--;
		console.log(`Couldn't get user ${target}`);
	}
};

let get_token = async function() {
	let cred;

	try {
		let res = await axios.post('https://api.intra.42.fr/oauth/token',
			{grant_type: 'client_credentials',
			client_id, client_secret});
		if (res.status == 200) {
			token = res.data.access_token;
			get_names();
		}
	} catch (e) {
		console.error(e);
		setTimeout(() => { get_token(); }, 10000);
	}
};

let print_map = function() {

	// For each campus
	for (let e in map) {

		console.log(`==========${e}==========`);

		// For each zone
		for (let z in map[e]) {
			console.log(`|=========${z}=========|`);

			for (let r in map[e][z]) {
				console.log(`||========${r}========||`);

				for (let p in map[e][z][r]) {
					let compl = '';
					for (var i = 0; i < 16 - map[e][z][r][p].length - p.length; i++)
						compl = `${compl} `;
					console.log(`|| ${p} ${map[e][z][r][p]}${compl}||`);
				}

				console.log(`||==================||`);
			}

			console.log(`|====================|`);
		}

		console.log(`======================`)
	}
};

let get_names = async function() {

	// const fd = fs.createReadStream(process.argv[2]);
	
	// const rl = readline.createInterface({
	// 	input: fd,
	// 	crlfDelay: Infinity
	// });

	// rl.on('line', async (line) => {
	// 	on_going++;
	// 	console.log(`Wait`);
	// 	await sleep(1000);
	// 	console.log(`Making request`);
	// 	await get_result(line);
	// 	if (on_going == 0)
	// 		print_map();
	// });

	let lines = fs.readFileSync(process.argv[2], 'utf-8').split(/\r?\n/);
	
	(async function() {
		for (const ln of lines) {
			if (ln) {
				on_going++;
				await get_result(ln);
				await sleep(250);
			}
		}
		print_map();
	})();
}

if (process.argv.length == 3)
	get_token();
else {
	console.error('Usage: node app <file_with_intra_names>');
	process.exit(1);
}
